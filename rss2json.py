from __future__ import print_function
import json, time, datetime

class FeedContentEncoder(json.JSONEncoder):   
    def default(self, obj):
        if isinstance(obj, time.struct_time):
            dt = datetime.datetime.fromtimestamp(time.mktime(obj))
            return {'__type__': '__datetime__', 'datetime': dt.isoformat()}
        else:
            return json.FeedContentEncoder.default(self, obj)

def dumps (feed, indent=None):
    return json.dumps(feed, cls=FeedContentEncoder, indent=indent)

def loads (src):
    """ Load the json object, restoring datetimes using dateutil.parser """
    import dateutil.parser
    def decode_feed_content(obj):
        if isinstance(obj, types.DictionaryType) and '__type__' in obj:
            if obj['__type__'] == '__datetime__':
                return dateutil.parser.parse(obj['datetime'])
        return obj
    return json.loads(obj, object_hook=decode_feed_content)


if __name__ == "__main__":
    from argparse import ArgumentParser
    ap = ArgumentParser("Use feedparser to parse RSS/Atom and dump as JSON")
    ap.add_argument('url')
    # ap.add_argument('--format', default="json")
    ap.add_argument("--indent", type=int, default=None)
    # ap.add_argument("--next", action="store_true", default=False)
    args = ap.parse_args()
    import feedparser
    fp = feedparser.parse(args.url)
    print (dumps(fp, indent=args.indent))
